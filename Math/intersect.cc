#include <cmath>
#include "intersect.h"
#include "constants.h"
#include "tools.h"

/* | algo           | difficulty | */
/* |----------------+------------| */
/* | BSPhereBSPhere |          1 | */
/* | BSPherePlane   |          2 | */
/* | BBoxBBox       |          3 | */
/* | BSphereBBox    |          5 | */
/* | BBoxPlane      |          5 | */


// @@ TODO: test if two BBoxes intersect.
//! Returns :
//    IINTERSECT intersect
//    IREJECT don't intersect

int BBoxBBoxIntersect(const BBox *bba, const BBox *bbb )
{
	return ((bba->m_max.x() < bbb->m_min.x()) || (bbb->m_max.x() < bba->m_min.x())) ||
				 ((bba->m_max.y() < bbb->m_min.y()) || (bbb->m_max.y() < bba->m_min.y())) ||
				 ((bba->m_max.z() < bbb->m_min.z()) || (bbb->m_max.z() < bba->m_min.z()));
}

// @@ TODO: test if a BBox and a plane intersect.
//! Returns :
//   +IREJECT outside
//   -IREJECT inside
//    IINTERSECT intersect

/*int  BBoxPlaneIntersect (const BBox *theBBox, Plane *thePlane)
{
	Vector3 centro = (theBBox->m_max + theBBox->m_min)/2;
	Vector3 h = theBBox->m_max - centro;

	float dist = thePlane->distance(centro);
	float extent = h.dot(thePlane->m_n);

	float res = dist - extent;
	return (res > 0) ? IREJECT : (res < 0) ? -IREJECT : IINTERSECT;
}*/
int  BBoxPlaneIntersect (const BBox *theBBox, Plane *thePlane)
{
	Vector3 Vmin;
	Vector3 Vmax;

	int min_side;
	int max_side;

	for(size_t i = 0; i < 3 ; ++i)
	{
		if (thePlane->m_n[i] >= 0.0f)
		{
			Vmin[i] = theBBox->m_min[i];
			Vmax[i] = theBBox->m_max[i];
		}
		else
		{
			Vmin[i] = theBBox->m_max[i];
			Vmax[i] = theBBox->m_min[i];
		}
	}
	min_side = thePlane->whichSide(Vmin);
	if (min_side > 0) return IREJECT;  // Vmin on possitive side: bbox outside
	max_side = thePlane->whichSide(Vmax);
	if (max_side < 0) return -IREJECT;  // Vmax on negative side: bbox inside
	return IINTERSECT; // Intersecting
}



// @@ TODO: test if two BSpheres intersect.
//! Returns :
//    IREJECT don't intersect
//    IINTERSECT intersect

int BSphereBSphereIntersect(const BSphere *bsa, const BSphere *bsb )
{
	float d = pow(bsa->getPosition().x() - bsb->getPosition().x(), 2) + pow(bsa->getPosition().y() - bsb->getPosition().y(), 2) + pow(bsa->getPosition().z() - bsb->getPosition().z(),2);
	return d > pow(bsa->getRadius()+bsb->getRadius(), 2);
}

// @@ TODO: test if a BSpheres intersects a plane.
//! Returns :
//   +IREJECT outside
//   -IREJECT inside
//    IINTERSECT intersect

int BSpherePlaneIntersect(const BSphere *bs, Plane *pl)
{
	float dist = pl->distance(bs->getPosition());
	return (dist > bs->getRadius()) ? IREJECT : (dist < bs->getRadius()) ? -IREJECT : IINTERSECT;
}

// @@ TODO: test if a BSpheres intersect a BBox.
//! Returns :
//    IREJECT don't intersect
//    IINTERSECT intersect

int BSphereBBoxIntersect(const BSphere *sphere, const BBox *box)
{
	float rad_2 = pow(sphere->getRadius(), 2);
	float dist_2 = 0;
	for(int i = 0; i < 3; i++)
	{
		if(sphere->m_centre[i] < box->m_min[i])
		{
			dist_2 += pow(sphere->m_centre[i] - box->m_min[i], 2);
		}
		else if(sphere->m_centre[i] > box->m_max[i])
		{
			dist_2 += pow(sphere->m_centre[i] - box->m_max[i], 2);
		}
	}

	return (dist_2>rad_2);
}


int IntersectTriangleRay(const Vector3 & P0,
						 const Vector3 & P1,
						 const Vector3 & P2,
						 const Line *l,
						 Vector3 & uvw) {
	Vector3 e1(P1 - P0);
	Vector3 e2(P2 - P0);
	Vector3 p(crossVectors(l->m_d, e2));
	float a = e1.dot(p);
	if (fabs(a) < Constants::distance_epsilon) return IREJECT;
	float f = 1.0f / a;
	// s = l->o - P0
	Vector3 s(l->m_O - P0);
	float lu = f * s.dot(p);
	if (lu < 0.0 || lu > 1.0) return IREJECT;
	Vector3 q(crossVectors(s, e1));
	float lv = f * q.dot(l->m_d);
	if (lv < 0.0 || lv > 1.0) return IREJECT;
	uvw[0] = lu;
	uvw[1] = lv;
	uvw[2] = f * e2.dot(q);
	return IINTERSECT;
}

/* IREJECT 1 */
/* IINTERSECT 0 */

const char *intersect_string(int intersect) {

	static const char *iint = "IINTERSECT";
	static const char *prej = "IREJECT";
	static const char *mrej = "-IREJECT";
	static const char *error = "IERROR";

	const char *result = error;

	switch (intersect) {
	case IINTERSECT:
		result = iint;
		break;
	case +IREJECT:
		result = prej;
		break;
	case -IREJECT:
		result = mrej;
		break;
	}
	return result;
}
