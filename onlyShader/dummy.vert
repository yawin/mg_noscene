#version 120

uniform mat4 modelToCameraMatrix; // M
uniform mat4 cameraToClipMatrix;  // P
// uniform mat4 modelToClipMatrix;   // MP

// uniform int active_lights_n; // < MG_MAX_LIGHT

// uniform struct light_t {
//   vec4 position;   // Camera space
//   vec4 ambient;
//   vec4 diffuse;
//   vec4 specular;
// } theLights[8];    // MG_MAX_LIGHTS

// uniform struct material_t {
//  vec4  ambient;
//  vec4  diffuse;
//  vec4  specular;
//  float shininess;
// } theMaterial;

attribute vec4 v_position;
// attribute vec3 v_normal;
// attribute vec2 v_texCoord;

varying vec4 f_color;
uniform float u_time;
uniform float u_step;

void main()
{
	float s = sin(u_time);
	float c = cos(u_time);
	float sc = 1-(s+c)/2;
	f_color = vec4(s, c, sc, 1);


	vec4 pos = v_position;
	pos = pos + vec4(sin(u_step), cos(u_step), 0.0, u_time);

  gl_Position = cameraToClipMatrix * modelToCameraMatrix * pos;
}
