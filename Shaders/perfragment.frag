#version 120

uniform int active_lights_n; // Number of active lights (< MG_MAX_LIGHT)
uniform vec3 scene_ambient; // Scene ambient light

uniform struct light_t {
	vec4 position;    // Camera space
	vec3 diffuse;     // rgb
	vec3 specular;    // rgb
	vec3 attenuation; // (constant, lineal, quadratic)
	vec3 spotDir;     // Camera space
	float cosCutOff;  // cutOff cosine
	float exponent;
} theLights[4];     // MG_MAX_LIGHTS

uniform struct material_t {
	vec3  diffuse;
	vec3  specular;
	float alpha;
	float shininess;
} theMaterial;

uniform sampler2D texture0;

varying vec3 f_position;      // camera space
varying vec3 f_viewDirection; // camera space
varying vec3 f_normal;        // camera space
varying vec2 f_texCoord;

float lambert_factor(const vec3 n, const vec3 l)
{
	return max(0.0, dot(n, l));
}

float specular_factor(const vec3 n, const vec3 l, const vec3 v, float m)
{
	float nl = dot(n, l);
	vec3 r = 2*(nl)*n - l;
	float rv = dot(r, v);

	return (rv > 0) ?  nl * pow(rv, m) : 0.0;
}

void direction_light(const in int i,
					 const in vec3 lightDirection,
					 const in vec3 viewDirection,
					 const in vec3 normal,
					 inout vec3 diffuse, inout vec3 specular)
{
	float NL = lambert_factor(normal, lightDirection);
	if(NL > 0.0)
	{
		diffuse += NL * theLights[i].diffuse * theMaterial.diffuse;

		float spec = specular_factor(normal, lightDirection, viewDirection, theMaterial.shininess);
		if(spec > 0)
		{
			specular += spec * theMaterial.specular * theLights[i].specular;
		}
	}
}

// Note: do not calculate the attenuation in point_lights

void point_light(const in int i,
				 const in vec3 position,
				 const in vec3 viewDirection,
				 const in vec3 normal,
				 inout vec3 diffuse, inout vec3 specular)
{
	//Distancia desde el vertice a la posicion de la luz
	vec3 distancia = theLights[i].position.xyz - position;

	//l = (Pl - Ps) / (|Pl - Ps|)
	vec3 lightDirection = normalize(distancia);

	//sqrt(x*x + y*y + z*z)
	float NL = lambert_factor(normal, lightDirection);

	if(NL > 0.0)
	{
		diffuse += NL * theLights[i].diffuse * theMaterial.diffuse;
		float spec = specular_factor(normal, lightDirection, viewDirection, theMaterial.shininess);
		if(spec > 0)
		{
			specular += spec * NL * theMaterial.specular * theLights[i].specular;
		}
	}
}

// Note: no attenuation in spotlights

void spot_light(const in int i,
				const in vec3 position,
				const in vec3 viewDirection,
				const in vec3 normal,
				inout vec3 diffuse, inout vec3 specular)
{
	vec3 lightDirection = normalize(theLights[i].position.xyz - position);

	float spDot = dot(-lightDirection, normalize(theLights[i].spotDir));
	float cspt = (spDot >= theLights[i].cosCutOff) ? pow(max(spDot, 0.0), theLights[i].exponent) : 0.0;

	float NL = lambert_factor(normal, lightDirection);
	if(NL > 0)
	{
		diffuse += NL * theMaterial.diffuse * theLights[i].diffuse * cspt;
		float spec = specular_factor(normal, lightDirection, viewDirection, theMaterial.shininess);
		if(spec > 0)
		{
			specular += spec * NL * theMaterial.specular * theLights[i].specular * cspt;
		}
	}
}

void main()
{
	//Acumuladores
	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);

	//Vector de posición convertido al sistema de referencias de la cámara
	vec4 P4 = vec4(f_position, 1.0);
	vec3 positionEye = -normalize(P4.xyz);

	vec4 N4 = vec4(f_normal, 0.0);
	vec3 normalEye = normalize(N4.xyz);

	vec3 L;

	for(int i=0; i < active_lights_n; ++i)
	{
		if(theLights[i].position.w == 0.0)
		{
			L = normalize(-theLights[i].position.xyz);
			direction_light(i, L, positionEye, normalEye, diffuse, specular);
		}
		else
		{
		  if (theLights[i].cosCutOff == 0.0)
			{
				point_light(i, P4.xyz, positionEye, normalEye, diffuse, specular);
		  }
			else
			{
				spot_light(i, P4.xyz, positionEye, normalEye, diffuse, specular);
		  }
		}
	}

	gl_FragColor = vec4(scene_ambient + diffuse + specular, 1.0) * texture2D(texture0, f_texCoord);
}
