#version 120

// Bump mapping with many lights.
//
// All computations are performed in the tangent space; therefore, we need to
// convert all light (and spot) directions and view directions to tangent space
// and pass them the fragment shader.

varying vec2 f_texCoord;
varying vec3 f_viewDirection;     // tangent space
varying vec3 f_lightDirection[4]; // tangent space
varying vec3 f_spotDirection[4];  // tangent space

// all attributes in model space
attribute vec3 v_position;
attribute vec3 v_normal;
attribute vec2 v_texCoord;
attribute vec3 v_TBN_t;
attribute vec3 v_TBN_b;

uniform mat4 modelToCameraMatrix;
uniform mat4 modelToWorldMatrix;
uniform mat4 cameraToClipMatrix;
uniform mat4 modelToClipMatrix;

uniform int active_lights_n; // Number of active lights (< MG_MAX_LIGHT)

uniform struct light_t {
	vec4 position;    // Camera space
	vec3 diffuse;     // rgb
	vec3 specular;    // rgb
	vec3 attenuation; // (constant, lineal, quadratic)
	vec3 spotDir;     // Camera space
	float cosCutOff;  // cutOff cosine
	float exponent;
} theLights[4];     // MG_MAX_LIGHTS

void main()
{
	vec3 L;

	mat3 MV3x3 = mat3(modelToCameraMatrix); // 3x3 modelview matrix
	mat3 TBN = transpose(MV3x3 * mat3(v_TBN_t, v_TBN_b, v_normal));

	vec4 f_position = modelToCameraMatrix * vec4(v_position, 1.0);

	f_viewDirection = TBN * (MV3x3 * normalize(-v_position));

	for(int i = 0; i < active_lights_n; i++)
	{
		if(theLights[i].position.w == 0.0)
		{
			L = -theLights[i].position.xyz;
		}
		else
		{
			L = theLights[i].position.xyz - f_position.xyz;
			f_spotDirection[i] = TBN * theLights[i].spotDir;
		}

		f_lightDirection[i] = TBN * L;
	}

	f_texCoord = v_texCoord;

	gl_Position = modelToClipMatrix * vec4(v_position, 1.0);

}
