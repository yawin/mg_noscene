#version 120

uniform int active_lights_n; // Number of active lights (< MG_MAX_LIGHT)
uniform vec3 scene_ambient; // Scene ambient light

struct material_t {
	vec3  diffuse;
	vec3  specular;
	float alpha;
	float shininess;
};

struct light_t {
	vec4 position;    // Camera space
	vec3 diffuse;     // rgb
	vec3 specular;    // rgb
	vec3 attenuation; // (constant, lineal, quadratic)
	vec3 spotDir;     // Camera space
	float cosCutOff;  // cutOff cosine
	float exponent;
};

uniform light_t theLights[4];
uniform material_t theMaterial;

uniform sampler2D texture0;
uniform sampler2D bumpmap;

varying vec2 f_texCoord;
varying vec3 f_viewDirection;     // tangent space
varying vec3 f_lightDirection[4]; // tangent space
varying vec3 f_spotDirection[4];  // tangent space

float lambert_factor(const vec3 n, const vec3 l)
{
	return max(0.0, dot(n, l));
}

float specular_factor(const vec3 n, const vec3 l, const vec3 v, float m)
{
	float nl = dot(n, l);
	vec3 r = 2*(nl)*n - l;
	float rv = dot(r, v);

	return (rv > 0) ?  max(0.0, nl) * pow(rv, m) : 0.0;
}

void direction_light(const in int i,
					 const in vec3 lightDirection,
					 const in vec3 viewDirection,
					 const in vec3 normal,
					 inout vec3 diffuse, inout vec3 specular)
{
	float NL = lambert_factor(normal, lightDirection);
	if(NL > 0.0)
	{
		diffuse += NL * theLights[i].diffuse * theMaterial.diffuse;

		float spec = specular_factor(normal, lightDirection, viewDirection, theMaterial.shininess);
		if(spec > 0)
		{
			specular += spec * theMaterial.specular * theLights[i].specular;
		}
	}
}

// Note: do not calculate the attenuation in point_lights
//
// Note: this function uses 'lightDirection', the direction of the light in
//    tangent space, instead of 'position' (as used in pervertex and
//    perfragment)

void point_light(const in int i,
				 const in vec3 position,
				 const in vec3 viewDirection,
				 const in vec3 normal,
				 inout vec3 diffuse, inout vec3 specular)
{
	//l = (Pl - Ps) / (|Pl - Ps|)
	vec3 lightDirection = position;

	//sqrt(x*x + y*y + z*z)
	float NL = lambert_factor(normal, lightDirection);

	if(NL > 0.0)
	{
		diffuse += NL * theLights[i].diffuse * theMaterial.diffuse;// * atenuacion;
		float spec = specular_factor(normal, lightDirection, viewDirection, theMaterial.shininess);
		if(spec > 0)
		{
			specular += spec * NL * theMaterial.specular * theLights[i].specular;// * atenuacion;
		}
	}
}

// Note: no attenuation in spotlights

void spot_light(const in int i,
				const in vec3 lightDirection,
				const in vec3 viewDirection,
				const in vec3 normal,
				inout vec3 diffuse, inout vec3 specular)
{
	float spDot = dot(-lightDirection, normalize(f_spotDirection[i]));
	float cspt = (spDot >= theLights[i].cosCutOff) ? pow(max(spDot, 0.0), theLights[i].exponent) : 0.0;

	float NL = lambert_factor(normal, lightDirection);
	if(NL > 0)
	{
		diffuse += NL * theMaterial.diffuse * theLights[i].diffuse * cspt;
		float spec = specular_factor(normal, lightDirection, viewDirection, theMaterial.shininess);
		if(spec > 0)
		{
			specular += spec * NL * theMaterial.specular * theLights[i].specular * cspt;
		}
	}
}


void main()
{
	//Acumuladores
	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);

	vec3 positionEye = normalize(f_viewDirection);

	vec4 mapaBump = texture2D(bumpmap, f_texCoord);
	vec3 normalEye = normalize(mapaBump.rgb * 2.0 - 1.0);

	vec3 L;

	for(int i=0; i < active_lights_n; ++i)
	{
			L = normalize(f_lightDirection[i]);
			if(theLights[i].position.w == 0.0)
			{
				direction_light(i, L, positionEye, normalEye, diffuse, specular);
			}
			else
			{
			  if(theLights[i].cosCutOff == 0.0)
				{
					point_light(i, L, positionEye, normalEye, diffuse, specular);
			  }
				else
				{
					spot_light(i, L, positionEye, normalEye, diffuse, specular);
			  }
			}
	}

	gl_FragColor = vec4(scene_ambient + diffuse + specular, 1.0) * texture2D(texture0, f_texCoord);
	//gl_FragColor = vec4(1.0);
}
